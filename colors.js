export default {
  cub: 0xff0000,
  oid: 0xffff00,
  octa: 0x00ff00,
  tetra: 0x0000ff,
  cubCss: '#ff0000',
  oidCss: '#ffff00',
  octaCss: '#00ff00',
  tetraCss: '#0000ff',

  grunt: 0x665544,
  warrior: 0x556655,
  archer: 0x888877,
  ogre: 0x555566,
  lemming: 0x665555,
  wizard: 0x665566,
  slime: 0x444444,
  slimet: 0x444444,
  doc: 0x777788,

  target: 0x00ff00,
  noTarget: 0xff0000,
  path: 0x33bb33,
  noPath: 0xbb3333,

  explosion: 0xeeee22
}