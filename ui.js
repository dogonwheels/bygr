import React from 'react';
import ReactDOM from 'react-dom';

import App from './ui/App';

class Ui {
  constructor(game) {
    this._game = game;
  }

  setStateMachine(stateMachine) {
    this._stateMachine = stateMachine;
  }

  _latest() {
    return {
      friends: this._game.friends,
      enemies: this._game.enemies,
      progress: this._game.progress,
      currentScreen: this._game.screen,
      screen: this.screen,
      currentFriend: this._game.currentFriend,
      currentAbility: this._game.currentAbility,
      level: this._game.level,
      send: (message, options) => { this._stateMachine.send(message, options); }
    };
  }

  render() {
    // FIXME subscribe to change
    ReactDOM.render(
      (<App game={this._latest()}/>),
      document.getElementById('root')
    );
  };
}


export default Ui;