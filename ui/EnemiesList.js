import React from 'react';
import Bar from './Bar';

const EnemiesList = (props) => {
  const style = {
    backgroundColor: 'rgba(64, 64, 64, 0.3)',
    width: 150,
    padding: 5,
    margin: 5
  };
  const deadStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.3)',
    width: 150,
    padding: 5,
    margin: 5,
    opacity: 0.5
  };

  return (
    <div>
      { props.enemies.map((enemy, index) => {
        return (
          <div style={enemy.isAlive() ? style : deadStyle} key={index}>
            <div className="panelTitleContainer">
              <div className="panelTitle">{enemy.name}</div>
            </div>
            <Bar total={enemy.totalHealth}
                 current={enemy.health}
                 color="#600"
                 symbol="heart"/>
            <Bar total={enemy.totalActionPoints}
                 current={enemy.remainingActionPoints}
                 color="#060"
                 symbol="energy"/>
          </div>
        );
      }) }
    </div>
  );
};

export default EnemiesList;
