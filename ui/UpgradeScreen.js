import React from 'react';

import _ from 'underscore';
import Select from '../screens/Select';
import UpgradeFriend from './UpgradeFriend';

const UpgradeScreen = React.createClass({
  componentWillMount: function() {
    this.props.game.friends.forEach((friend) => {
      friend.toApply = {
        vitality: 0,
        toughness: 0,
        strength: 0,
        agility: 0
      };
    })
  },

  onContinueClick: function() {
    this.props.game.friends.forEach((friend) => {
      const { vitality, toughness, strength, agility } = friend.toApply;
      const totalSpend = vitality + toughness + strength + agility;
      friend.stats.xpToSpend -= totalSpend;
      friend.stats.vitality += vitality;
      friend.stats.strength += strength;
      friend.stats.toughness += toughness;
      friend.stats.agility += agility;
    });

    this.props.game.screen.change(new Select());
  },

  render: function() {
    const style = {
      position: 'fixed',
      color: 'white',
      width: '100%'
    };
    const header = {};

    return (
      <div style={style} className="screen">
        <div style={header}>
          <h1>Upgrade your heroes</h1>
        </div>
        <div className="upgradeFriends">
          {this.props.game.friends.map((friend, index) => (
            <UpgradeFriend friend={friend} key={index}/>
          ))}
        </div>
        <button onClick={_.bind(this.onContinueClick, this)}>Continue</button>
      </div>
    );
  }
});

export default UpgradeScreen;