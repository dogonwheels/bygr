import React from 'react';

import Upgrade from '../screens/Upgrade';
import Welcome from '../screens/Welcome';

import AbilitiesList from './AbilitiesList';
import FriendsList from './FriendsList';
import EnemiesList from './EnemiesList';

const PlayScreen = React.createClass({
  render: function() {
    const style = {
      position: 'fixed',
      color: 'white'
    };

    const friends = {
      position: 'fixed',
      top: 20,
      left: 20
    };

    const abilities = {
      position: 'fixed',
      top: 20,
      left: 200
    };

    const enemies = {
      position: 'fixed',
      top: 20,
      right: 20
    };

    const bottom = {
      position: 'fixed',
      bottom: 20,
      right: 20
    };

    const ended = {
      position: 'fixed',
      width: '100%',
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: '72'
    };

    const onEndClick = () => {
      this.props.game.send('EndTurn');
    };

    const onVictoryClick = () => {
      this.props.game.screen.change(new Upgrade());
    };

    const onDefeatClick = () => {
      this.props.game.screen.change(new Welcome());
    };

    const defeat = this.props.game.progress === 'defeat';
    const victory = this.props.game.progress === 'victory';

    const hasEnded = defeat || victory;

    return (
      <div style={style} className="screen">
        {victory && <div style={ended} onClick={onVictoryClick}>Much Success!</div>}
        {defeat && <div style={ended} onClick={onDefeatClick}>Much Defeat!</div>}
        <div style={friends}>
          <FriendsList
            send={this.props.game.send}
            current={this.props.game.currentFriend}
            friends={this.props.game.friends}
          />
        </div>
        <div style={abilities}>
          <AbilitiesList
            send={this.props.game.send}
            current={this.props.game.currentAbility}
            currentFriend={this.props.game.currentFriend}
            abilities={this.props.game.currentFriend.abilities}
          />
        </div>
        <div style={enemies}>
          <EnemiesList
            enemies={this.props.game.enemies}
          />
        </div>
        <div style={bottom}>
          <button onClick={onEndClick}>End Turn</button>
        </div>
      </div>
    );
  }
});

export default PlayScreen;