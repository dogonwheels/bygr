import React from 'react';

import PlayScreen from './PlayScreen';
import SelectScreen from './SelectScreen';
import UpgradeScreen from './UpgradeScreen';
import WelcomeScreen from './WelcomeScreen';

const App = React.createClass({
  render: function() {
    const screens = {
      welcome: WelcomeScreen,
      select: SelectScreen,
      play: PlayScreen,
      upgrade: UpgradeScreen
    };

    const Element = screens[this.props.game.currentScreen];

    return (
      <div className="app">
        { React.createElement(Element, { game: this.props.game }) }
      </div>
    );
  }
});

export default App;