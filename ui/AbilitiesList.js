import React from 'react';

import Bar from './Bar';

const AbilitiesList = (props) => {
  const unselectedStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.5)',
    padding: 5,
    margin: 5,
    width: 150
  };
  const selectedStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.8)',
    width: 150,
    padding: 5,
    margin: 5
  };
  const unavailableStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.5)',
    width: 150,
    padding: 5,
    margin: 5,
    opacity: 0.7
  };
  const insufficientStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.5)',
    padding: 5,
    width: 150,
    margin: 5,
    opacity: 0.5
  };

  const handler = (ability) => {
    return () => {
      if (ability.isAvailable() && ability.isSufficientLevel()) {
        props.send('SelectAbility', {ability});
      }
    }
  };

  const titleStyle = {
    width: '100%',
    height: 20,
    padding: 5,
    position: 'relative'
  };

  const titleName = {
    position: 'absolute',
    left: 0
  };
  const titleCost = {
    position: 'absolute',
    right: 10
  };

  const current = props.current;
  return (
    <div>
      { props.abilities.map((ability, index) => {
        let style = ability === current ? selectedStyle : unselectedStyle;
        let cost = ability.minimumActionPoints;
        if (!ability.isAvailable()) {
          style = unavailableStyle;
        }
        if (!ability.isSufficientLevel()) {
          style = insufficientStyle;
          cost = `${ability.minimumXp()}XP`;
        }

        return (
          <div key={index}
               style={style}
               className={ability.isAvailable() && 'clickable'}
               onClick={handler(ability)}>
            <div style={titleStyle}>
              <div style={titleName}>{ability.name}</div>
              <div style={titleCost}>{cost}</div>
            </div>
            { !!ability.minimumSpentActionPoints && !!ability.isSufficientLevel() && (
              <Bar total={ability.minimumSpentActionPoints}
                   current={Math.min(ability.minimumSpentActionPoints, ability.spentActionPoints)}
                   color="#006"
                   symbol="reload"/>
            ) }
          </div>
        );
      })}
    </div>
  );
};

export default AbilitiesList;