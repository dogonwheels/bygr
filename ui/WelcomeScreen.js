import React from 'react';

import Select from '../screens/Select';

const WelcomeScreen = React.createClass({
  render: function() {
    const onStartClick = () => {
      this.props.game.screen.change(new Select());
    };
    const style = {
      color: 'white',
      position: 'fixed',
      width: '100%',
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      fontSize: '72'
    };

    return (
      <div style={style} className="screen">
        <div>bygr</div>
        <div><button onClick={onStartClick}>Start</button></div>
      </div>
    );
  }
});

export default WelcomeScreen;