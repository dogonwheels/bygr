import React from 'react';

const Bar = (props) => {
  const fraction = props.current / props.total;

  const barStyle = {
    display: 'flex',
    padding: '5px'
  };
  const label = {
    width: 20
  };
  const fullStyle = {
    flexGrow: 1,
    height: '20px',
    background: 'black',
    position: 'relative'
  };
  const partialStyle = {
    width: `${fraction * 100}%`,
    height: '20px',
    background: props.color
  };
  const currentStyle = {
    position: 'absolute',
    left: 0,
    top: 0
  };
  const totalStyle = {
    position: 'absolute',
    top: 0,
    right: 0
  };

  const img = `images/${props.symbol}.svg`;

  return (
    <div style={barStyle}>
      <div style={label}>
        <img src={img} style={{ width: '20px' }}/>
      </div>
      <div style={fullStyle}>
        <div style={partialStyle}>
        </div>
        <div style={currentStyle}>{props.current}</div>
        <div style={totalStyle}>{props.total}</div>
      </div>
    </div>
  )
};

export default Bar;

