import React from 'react';
import Bar from './Bar';

function renderStat(stats, stat) {
  const img = `images/${stat}.svg`;
  return (
    <div className="stat">
      <img src={img} style={{width: '20'}}/>
      <div className="statText">{stats[stat]}</div>
    </div>
  )
}

const FriendsList = (props) => {
  const style = {
    backgroundColor: 'rgba(64, 64, 64, 0.5)',
    padding: 5,
    margin: 5
  };
  const selectedStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.8)',
    padding: 5,
    margin: 5
  };
  const deadStyle = {
    backgroundColor: 'rgba(64, 64, 64, 0.5)',
    padding: 5,
    margin: 5,
    opacity: 0.7
  };

  const handler = (friend) => {
    return () => {
      if(friend.isAlive()) {
        props.send('SelectFriend', { friend });
      }
    }
  };

  const current = props.current;

  const colorBox = (friend) => ({
    width: 20,
    height: 20,
    paddingRight: 5,
    background: friend.cssColor,
    display: 'inline-block'
  });

  const friendsStyle = {
  };

  return (
    <div>
      <div style={friendsStyle}>
      { props.friends.map((friend, index) => {
        let s = (friend === current) ? selectedStyle : style;
        if (!friend.isAlive()) {
          s = deadStyle;
        }

          return (
            <div style={s}
                 className={friend.isAlive() && 'clickable'}
                 key={index}
                 onClick={handler(friend)}>
              <div className="panelTitleContainer">
                <div style={colorBox(friend)}></div>
                <div className="panelTitle">{friend.name}</div>
                <div className="panelSub">{friend.stats.xp}XP</div>
              </div>
              <div className="stats">
                {renderStat(friend.stats, 'agility')}
                {renderStat(friend.stats, 'vitality')}
                {renderStat(friend.stats, 'strength')}
                {renderStat(friend.stats, 'toughness')}
              </div>

              <Bar total={friend.totalHealth} current={friend.health} color="#600"
                   symbol="heart"/>
              <Bar total={friend.totalActionPoints}
                   current={friend.potentialActionPoints} color="#060"
                   symbol="energy"/>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default FriendsList;
