import React from 'react';

import Play from '../screens/Play';

const SelectScreen = React.createClass({
  render: function() {
    const onPlayClick = (level) => (() => {
      // FIXME: only safe to mutate game state as we exit this screen
      this.props.game.level.difficulty = level;
      this.props.game.screen.change(new Play());
    });
    const style = {
      color: 'white',
      position: 'fixed',
      width: '100%',
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      fontSize: '24'
    };

    return (
      <div style={style} className="screen">
        <h1>Choose your next adventure</h1>
        <h2>(More danger means more experience!)</h2>
        <div>
            <button onClick={onPlayClick(1)}>Easy</button>
            <button onClick={onPlayClick(2)}>Normal</button>
            <button onClick={onPlayClick(3)}>Hard</button>
        </div>
      </div>
    );
  }
});

export default SelectScreen;