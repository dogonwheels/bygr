import React from 'react';
import _ from 'underscore';

const UpgradeFriend = React.createClass({
  getInitialState() {
    return {
      agility: 0,
      vitality: 0,
      strength: 0,
      toughness: 0
    };
  },

  onReduce(stat) {
    return () => {
      const newValue = this.state[stat] - 1;
      this.setState({ [stat]: newValue });
      this.props.friend.toApply[stat] = newValue;
    };
  },

  onIncrease(stat) {
    return () => {
      const newValue = this.state[stat] + 1;
      this.setState({ [stat]: newValue });
      this.props.friend.toApply[stat] = newValue;
    };
  },

  renderButtons(stat, left) {
    const img = `images/${stat}.svg`;
    return (
      <div className="upgradeStat">
        <div className="label">
          <img src={img} style={{width: '20'}}/>
          {stat}: { this.props.friend.stats[stat] + this.state[stat] }
        </div>
        <div className="buttons">
          { (this.state[stat] !== 0) && <button onClick={_.bind(this.onReduce(stat), this)}>-</button> }
          { (left !== 0) && <button onClick={_.bind(this.onIncrease(stat), this)}>+</button> }
        </div>
      </div>
    );
  },

  render: function() {
    const { friend } = this.props;
    const statsToSpend = friend.stats.xpToSpend;
    const { agility, vitality, strength, toughness } = this.state;
    const spent = agility + vitality + strength + toughness;
    const left = statsToSpend - spent;

    const title = {
      width: '100%',
      background: friend.cssColor,
    };

    return (
      <div className='upgradeFriend'>
        <h2 style={title}>{ friend.name }</h2>
        <div>XP: { friend.stats.xp }</div>
        <div>Points: { left} / { statsToSpend }</div>

        <div className="upgradeStats">
          { this.renderButtons.call(this, 'agility', left) }
          { this.renderButtons.call(this, 'vitality', left) }
          { this.renderButtons.call(this, 'strength', left) }
          { this.renderButtons.call(this, 'toughness', left) }
        </div>

      </div>
    );
  }
});

export default UpgradeFriend;