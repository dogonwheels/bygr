class Screen {
  constructor(initialScreen, game, state) {
    this._game = game;
    this._state = state;

    this.current = initialScreen;
    this.current.enter(game, state, this);
  }

  change(newScreen) {
    if (newScreen !== this.current) {
      this.current.exit();
      this.current = newScreen;
      this.current.enter(this._game, this._state, this);
    }
  }
}

export default Screen;
