import THREE from 'three';
import _ from 'underscore';

import SetupFriendAbility from '../states/SetupFriendAbility';

import FlatCamera from '../entities/FlatCamera';
import Terrain from '../entities/Terrain';
import Overlays from '../entities/Overlays';

class Play {
  _pick({x, y}) {
    // FIXME: the rotate away from screen throws our FlatCamera raycaster :/
    const mouse = {
      x: (x / window.innerWidth) * 2 - 1,
      y: -(y / window.innerHeight) * 4 + 4.32 + (1 / this.game.mapSize)
    };
    this.raycaster.setFromCamera(mouse, this.camera);
    const intersects = this.raycaster.intersectObjects([this.terrain.land]);

    if (intersects.length) {
      return intersects[0].point;
    }
  };
  _onMouseMove({ clientX, clientY }) {
    const result = this._pick({x: clientX, y: clientY});
    if (result) {
      this.state.send('TileHover', {
        x: Math.round(result.x),
        y: Math.round(result.y)
      });
    } else {
      this.state.send('GameHover');
    }
  }

  _onMouseDown({ clientX, clientY }) {
    const result = this._pick({x: clientX, y: clientY});
    if (result) {
      this.state.send('TileSelect', {
        x: Math.round(result.x),
        y: Math.round(result.y)
      });
    }
  }

  enter(game, state, screen) {
    this.onMouseMove = _.bind(this._onMouseMove, this);
    this.onMouseDown = _.bind(this._onMouseDown, this);

    this.state = state;
    this.game = game;
    game.screen = 'play';

    this.scene = new THREE.Scene();
    this.camera = new FlatCamera(game.mapSize);

    const seed = Math.random() * 50000;
    game.startLevel(seed);

    const terrain = new Terrain(game.mapSize, game.heightField);
    this.terrain = terrain;

    const overlays = new Overlays();

    const startState = new SetupFriendAbility({
      game,
      overlays,
      scene: this.scene
    }, game.friends[0]);
    state.transition(startState);

    // Add meshes
    game.friends.forEach(({ mesh }) => {
      this.scene.add(mesh);
    });
    game.enemies.forEach(({ mesh }) => {
      this.scene.add(mesh);
    });

    this.scene.add(terrain.mesh);
    this.scene.add(overlays.mesh);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    directionalLight.position.set(0.2, -2, 3);
    this.scene.add(directionalLight);

    const directionalLight2 = new THREE.DirectionalLight(0xffffff, 0.3);
    directionalLight2.position.set(-1, -1, 1);
    this.scene.add(directionalLight2);

    this.raycaster = new THREE.Raycaster();

    window.addEventListener('mousemove', this.onMouseMove, false);
    window.addEventListener('mousedown', this.onMouseDown, false);
  }

  update() {

  }

  exit() {
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mousedown', this.onMouseDown);
  }
}

export default Play;