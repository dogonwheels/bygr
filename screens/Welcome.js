import Idle from '../states/Idle';

class Welcome {
  enter(game, state, screen) {
    game.screen = 'welcome';
    game.dirty();

    const startState = new Idle({game});
    state.transition(startState);
  }

  update() {
  }

  exit() {
  }
}

export default Welcome;