import Idle from '../states/Idle';

class Select {
  enter(game, state, screen) {
    game.screen = 'upgrade';
    game.dirty();

    const startState = new Idle({game});
    state.transition(startState);
  }

  update() {
  }

  exit() {
  }
}

export default Select;