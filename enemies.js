import Hit from './behaviours/Hit';
import MoveToNearest from './behaviours/MoveToNearest';
import MoveToWeakest from './behaviours/MoveToWeakest';
import MoveToAlly from './behaviours/MoveToWeakest';
import Retreat from './behaviours/Retreat';
import Explore from './behaviours/Explore';
import Explode from './behaviours/Explode';
import Arrow from './behaviours/Arrow';

/*
 stats:      [ A T S V  Cost ]
 abilities:  [[ BehaviourClass, scale, range? ]...]
 */
const enemies = {
  grunt: {
    stats: [4, 2, 2, 2],
    abilities: [
      [Hit, 1],
      [MoveToNearest, 1, 10],
      [Explore, 1]
    ]
  },
  warrior: {
    stats: [4, 6, 6, 4],
    abilities: [
      [Hit, 1],
      [MoveToWeakest, 1, 10],
      [Explore, 1]
    ]
  },
  archer: {
    stats: [4, 4, 6, 4],
    abilities: [
      [Arrow, 1, 8],
      [Retreat, 1, 6],
      [Hit, 1],
      [Explore, 1]
    ]
  },
  ogre: {
    stats: [2, 8, 8, 6],
    abilities: [
      [Hit, 1],
      [MoveToNearest, 1, 4],
      [Explore, 1]
    ]
  },
  lemming: {
    stats: [4, 2, 6, 2],
    abilities: [
      [Explode, 10],
      [MoveToNearest, 1, 15],
      [Explore, 1]
    ]
  },
  wizard: {
    stats: [6, 4, 2, 6],
    abilities: [
      [Retreat, 1, 10],
      //[Fireball, 1, 10],
      [Hit, 1],
      [Explore, 1]
    ]
  },
  slime: {
    stats: [8, 2, 2, 4],
    abilities: [
      //[Split, 1],
      [Hit, 1],
      [MoveToAlly, 1],
      [MoveToNearest, 1, 10],
      [Explore, 1]
    ]
  },
  slimet: {
    stats: [4, 2, 2, 2],
    abilities: [
      [Hit, 1],
      [MoveToAlly, 1],
      [MoveToNearest, 1, 10],
      [Explore, 1]
    ]
  },
  doc: {
    stats: [4, 4, 4, 6],
    abilities: [
      [Hit, 1],
      //[Heal, 1],
      [MoveToAlly, 1],
      [Retreat, 1, 10],
      [Explore, 1]
    ]
  }
};

export default enemies;
