import ROT from 'rot-js';

import Actor from './Actor';

class Friend extends Actor {
  constructor(game, who) {
    super(game, who);

    this.stats.xp = 0;
    this.stats.xpToSpend = 0;

    this.abilitySpecs = who.abilities.map((specs) => ({
      scale: specs[1],
      minimumXp: specs[2],
      level: 1
    }));

    this.abilities = who.abilities.map((specs, index) => (
      new specs[0](game, this, this.abilitySpecs[index])
    ));
  }

  reset() {
    // Per level
    // FIXME: base these of this._stats.vitality
    this.totalActionPoints = 1000;
    this.totalHealth = 100;

    this.mesh.scale.set(1, 1, 1);

    this.health = this.totalHealth;
    this.resetActionPoints();
    this.abilities.forEach((ability) => (ability.reset()));
  }

  previewActionPoints(spend) {
    this.potentialActionPoints = this.remainingActionPoints - spend;
    this._game.dirty(); // FIXME: notify
  }
}

export default Friend;