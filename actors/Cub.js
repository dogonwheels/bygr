import THREE from 'three';
import Friend from './Friend';

import friends from '../friends';
import colors from '../colors';

class Cub extends Friend {
  constructor(game) {
    super(game, friends.cub);

    this.color = colors.cub;
    this.cssColor = colors.cubCss;

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.name = 'Cub';
    this.mesh = new THREE.Mesh(geometry, material);
  }
}

export default Cub;
