import THREE from 'three';
import colors from '../colors';
import enemies from '../enemies';
import Actor from './Actor';

class Enemy extends Actor {
  constructor(game, who) {
    super(game, who);

    this.abilities = who.abilities.map((specs, index) => (
      new specs[0](game, this, { scale: specs[1], range: specs[2] })
    ));
  }

  reset() {
    this.totalActionPoints = 800 + (50 * this.stats.agility);
    this.totalHealth = 60 + (20 * this.stats.vitality);
    this.health = this.totalHealth;
    this.resetActionPoints();
  }
}

class Grunt extends Enemy {
  constructor(game, count) {
    super(game, enemies.grunt);
    this.color = colors.grunt;
    this.name = `Grunt ${count}`;

    const geometry = new THREE.BoxGeometry(0.5, 0.5, 0.5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = -0.25;
  }
}

class Warrior extends Enemy {
  constructor(game, count) {
    super(game, enemies.warrior);
    this.color = colors.warrior;
    this.name = `Warrior ${count}`;

    const geometry = new THREE.BoxGeometry(0.75, 0.75, 0.75);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = -0.125;
  }
}

class Archer extends Enemy {
  constructor(game, count) {
    super(game, enemies.archer);
    this.color = colors.archer;
    this.name = `Archer ${count}`;

    const geometry = new THREE.BoxGeometry(0.5, 0.5, 1.2);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = 0.1;
  }
}

class Ogre extends Enemy {
  constructor(game, count) {
    super(game, enemies.ogre);
    this.color = colors.ogre;
    this.name = `Ogre ${count}`;

    const geometry = new THREE.BoxGeometry(1, 1, 1.5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = 0.25;
  }
}

class Lemming extends Enemy {
  constructor(game, count) {
    super(game, enemies.lemming);
    this.color = colors.lemming;
    this.name = `Lemming ${count}`;

    const geometry = new THREE.IcosahedronGeometry(0.5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = 0;
  }
}

class Wizard extends Enemy {
  constructor(game, count) {
    super(game, enemies.wizard);
    this.color = colors.wizard;
    this.name = `Wizard ${count}`;

    const geometry = new THREE.CylinderGeometry(0, 0.6, 1, 6);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });


    this.mesh = new THREE.Group();
    const cone = new THREE.Mesh(geometry, material);
    cone.rotateX(Math.PI / 2);
    this.mesh.add(cone);
  }
}

class Slime extends Enemy {
  constructor(game, count) {
    super(game, enemies.slime);
    this.color = colors.slime;
    this.name = `Slime ${count}`;

    const geometry = new THREE.BoxGeometry(0.8, 0.8, 0.1);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = -0.45;
  }
}

class Slimet extends Enemy {
  constructor(game, count) {
    super(game, enemies.slimet);
    this.color = colors.slimet;
    this.name = `Slimet ${count}`;

    const geometry = new THREE.BoxGeometry(0.5, 0.5, 0.1);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = -0.45;
  }
}

class Doc extends Enemy {
  constructor(game, count) {
    super(game, enemies.doc);
    this.color = colors.doc;
    this.name = `Doc ${count}`;

    const geometry = new THREE.TorusGeometry(0.3, 0.3, 8, 5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = 0;
  }
}

export { Grunt, Warrior, Archer, Ogre, Lemming, Wizard, Slime, Slimet, Doc };
