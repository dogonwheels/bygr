import THREE from 'three';
import Friend from './Friend';

import friends from '../friends';
import colors from '../colors';

class Tetra extends Friend {
  constructor(game, evil) {
    super(game, friends.tetra);

    this.color = colors.tetra;
    this.cssColor = colors.tetraCss;

    const geom = new THREE.Geometry();
    geom.vertices.push(new THREE.Vector3(0, 0, 0.5));
    geom.vertices.push(new THREE.Vector3(-0.5, -0.5, -0.5));
    geom.vertices.push(new THREE.Vector3(0.5, -0.5, -0.5));
    geom.vertices.push(new THREE.Vector3(0.5, 0.5, -0.5));
    geom.vertices.push(new THREE.Vector3(-0.5, 0.5, -0.5));

    geom.faces.push(new THREE.Face3(0, 1, 2));
    geom.faces.push(new THREE.Face3(0, 2, 3));
    geom.faces.push(new THREE.Face3(0, 3, 4));
    geom.faces.push(new THREE.Face3(0, 4, 1));
    geom.computeFaceNormals();

    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.name = evil ? 'Artet' : 'Tetra';
    this.mesh = new THREE.Group();

    const character = new THREE.Mesh(geom, material);
    this.mesh.add(character);
  }
}

export default Tetra;