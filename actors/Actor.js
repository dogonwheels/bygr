import ROT from 'rot-js';

class Actor {
  constructor(game, who) {
    this._game = game;
    this._map = game.map;

    this.stats = {
      agility: who.stats[0],
      toughness: who.stats[1],
      strength: who.stats[2],
      vitality: who.stats[3]
    };

    this.wounding = Math.pow(Math.E, -(0.03 * this.stats.toughness));
  }

  resetActionPoints() {
    // Per turn
    this.abilities.forEach((ability) => {
      ability.spentActionPoints += this.remainingActionPoints;
    });
    this.remainingActionPoints = this.totalActionPoints;
    this.potentialActionPoints = this.remainingActionPoints;
    this._game.dirty();
  }

  useActionPoints(currentAbility, spend) {
    if (spend <= this.remainingActionPoints) {
      this.remainingActionPoints -= spend;
      this.potentialActionPoints = this.remainingActionPoints;

      // Add action points to every ability for cooldown/reload detection
      currentAbility.spentActionPoints = 0;
      this.abilities.forEach((ability) => {
        ability.spentActionPoints += spend;
      });

      this._game.dirty();
    }
  }

  availableAbilities() {
    // FIXME: move to an ability helper once we have BaseAbility
    return this.abilities.filter((ability) => {
      const enough = ability.minimumActionPoints <= this.remainingActionPoints;
      const waiting = ability.spentActionPoints < ability.minimumSpentActionPoints;

      return enough && !waiting;
    });
  }

  isAlive() {
    return this.health > 0;
  }

  hit(damage) {
    this.health -= Math.ceil(damage * this.wounding);

    if (this.health <= 0) {
      this._map.occupied.set({x: this.x, y: this.y}, false);
      this.health = 0;
    } else {
      this._map.occupied.set({x: this.x, y: this.y}, true);
      this.mesh.visible = true;
    }
  }

  moveTo({x, y}) {
    this._map.occupied.set({x: this.x, y: this.y}, false);
    this.x = x;
    this.y = y;

    this.mesh.position.x = x;
    this.mesh.position.y = y;

    this._map.occupied.set({x, y}, true);
    this.mesh.visible = true;
  }

  adjacent(destination) {
    // 4 way adjacency
    const dx = destination.x - this.x;
    const dy = destination.y - this.y;

    const ax = Math.abs(dx);
    const ay = Math.abs(dy);

    return (ax + ay) === 1;
  }

  line(destination) {
    const dx = destination.x - this.x;
    const dy = destination.y - this.y;

    const ax = Math.abs(dx);
    const ay = Math.abs(dy);

    const sx = dx / ax;
    const sy = dy / ay;

    const path = [{ x: this.x, y: this.y }];
    const current = { x: this.x, y: this.y };

    let error = 0.5;
    if (ax > ay) {
      // Step through x
      const g = ay / ax;
      for (let x = this.x; x !== destination.x; x+=sx) {
        error += g;
        if (error > 0.99) {
          error--;
          current.y += sy;
        }
        current.x += sx;
        path.push({x: current.x, y: current.y});
      }
    } else {
      const g = ax / ay;
      for (let y = this.y; y !== destination.y; y+=sy) {
        error += g;
        if (error > 0.99) {
          error--;
          current.x += sx;
        }
        current.y += sy;
        path.push({x: current.x, y: current.y});
      }
    }

    return path;
  }

  lineOfSight(destination) {
    const occupied = this._game.map.occupied;

    const path = this.line(destination);
    const lineOfSight = [];
    let blocked = false;
    for (let i = 1; !blocked && (i < path.length); i++) {
      const cell = path[i];
      blocked = occupied.get(cell);

      if (!blocked || (cell.x === destination.x && cell.y === destination.y)) {
        lineOfSight.push(cell);
      }
    }

    return lineOfSight;
  }

  path(destination, ignoreEnd = true) {
    const occupied = this._game.map.occupied;
    const astar = new ROT.Path.AStar(
      this.x,
      this.y,
      (x, y) => {
        const start = x === this.x && y === this.y;
        const end = x === destination.x && y === destination.y;
        return start || (ignoreEnd && end) || !occupied.get({ x, y });
      },
      { topology: 4 }
    );

    const path = [];
    astar.compute(
      destination.x,
      destination.y,
      (x, y) => { path.push({x, y}); }
    );

    return path.reverse();
  }
}

export default Actor;