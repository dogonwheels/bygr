import THREE from 'three';
import Friend from './Friend';

import friends from '../friends';
import colors from '../colors';

class Octa extends Friend {
  constructor(game) {
    super(game, friends.octa);
    this.color = colors.octa;
    this.cssColor = colors.octaCss;

    const geometry = new THREE.OctahedronGeometry(0.5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.name = 'Octa';
    this.mesh = new THREE.Group();

    const character = new THREE.Mesh(geometry, material);
    character.scale.set(1, 1, 2);
    character.position.z = 0.5;
    character.rotation.z = Math.PI / 4;
    this.mesh.add(character);
  }
}

export default Octa;