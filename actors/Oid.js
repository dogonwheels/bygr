import THREE from 'three';
import Friend from './Friend';

import friends from '../friends';
import colors from '../colors';

class Oid extends Friend {
  constructor(game, evil) {
    super(game, friends.oid);

    this.color = colors.oid;
    this.cssColor = colors.oidCss;

    const geometry = new THREE.BoxGeometry(0.75, 0.75, 1.5);
    const material = new THREE.MeshPhongMaterial({
      color: this.color,
      shading: THREE.FlatShading
    });

    this.name = evil ? 'Dio' : 'Oid';
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = 0.25;
  }
}

export default Oid;