import THREE from 'three';

class Bullet {
  constructor() {
    const geometry = new THREE.BoxGeometry(0.2, 0.2, 0.2);
    const material = new THREE.MeshBasicMaterial({ color: 0xffffff });

    this.mesh = new THREE.Mesh(geometry, material);
  }
}

export default Bullet;