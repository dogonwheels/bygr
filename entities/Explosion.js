import THREE from 'three';
import colors from '../colors';

class Explosion {
  constructor() {
    const geometry = new THREE.IcosahedronGeometry(1);
    const material = new THREE.MeshPhongMaterial({
      color: colors.explosion,
      opacity: 1.0,
      transparent: true,
      shading: THREE.FlatShading,
      shininess: 20
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.z = -0.5;
  }
}

export default Explosion;