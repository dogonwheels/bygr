import _ from 'underscore';
import Grid from '../Grid';
import Random from 'random-js'

class SpawnPoints {
  constructor(free, seed) {
    this._size = free.width;
    this._canSpawn = new Grid(this._size, false);

    this.mt = Random.engines.mt19937();
    this.mt.seed(seed);

    for (let y = 0; y < this._size; y++) {
      for (let x = 0; x < this._size; x++) {
        this._canSpawn.set({x, y}, free.get({x, y}));
      }
    }
  }

  _randomPoint() {
    return {
      x: Random.integer(0, this._size - 1)(this.mt),
      y: Random.integer(0, this._size - 1)(this.mt)
    };
  }

  fetchSpot(amount) {
    const result = [];
    // Random spots
    while (result.length !== amount) {
      let spawn;
      do {
        spawn = this._randomPoint();
      }
      while (!this._canSpawn.get(spawn));

      this._canSpawn.set(spawn, false);
      result.push(spawn);
    }
    return result;
  }

  fetchArea(amount, size, attempts = 3) {
    do {
      const spawn = this._randomPoint();
      const available = [];
      for (let dy = 0; dy < size; dy++) {
        for (let dx = 0; dx < size; dx++) {
          const pos = { x: spawn.x + dx, y: spawn.y + dy };
          if (this._canSpawn.get(pos)) {
            available.push(pos);
          }
        }
      }

      if (available.length >= amount) {
        return Random.sample(this.mt, available, amount);
      }

      attempts--;
    } while (attempts);

    return [];
  }

  fetch(amount, area=10) {
    let result = [];
    const roll = Random.real(0, 1)(this.mt);

    if (roll > 0.5) {
      result = this.fetchArea(amount, area);
    }

    if (!result.length && roll > 0.2) {
      result = this.fetchArea(amount, Math.round(2 * area / 3));
    }

    if (!result.length && roll > 0.05) {
      result = this.fetchArea(amount, Math.round(area / 4));
    }

    if (!result.length) {
      result = this.fetchSpot(amount);
    }

    result.forEach((position) => {
      this._canSpawn.set(position, false);
    });

    return result;
  }
}

export default SpawnPoints;
