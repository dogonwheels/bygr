import THREE from 'three';

class Overlays {
  constructor(scene) {
    this._pathGeometry = new THREE.PlaneGeometry(0.5, 0.5);
    this._pathMaterial = new THREE.MeshBasicMaterial({ color: 0x000088 } );

    const geometryMarker = new THREE.PlaneGeometry(1.2, 1.2);

    const startMaterial = new THREE.MeshPhongMaterial({
      opacity: 0.3,
      transparent: true,
      shading: THREE.FlatShading
    });
    const endMaterial = new THREE.MeshPhongMaterial({
      opacity: 0.5,
      transparent: true,
      shading: THREE.FlatShading
    });
    this._start = new THREE.Mesh(geometryMarker, startMaterial);
    this._end = new THREE.Mesh(geometryMarker, endMaterial);

    this._end.position.z = 0.01;
    this._start.position.z = 0.01;

    this._start.visible = false;
    this._end.visible = false;

    this.mesh = new THREE.Group();
    this.mesh.add(this._start);
    this.mesh.add(this._end);

    this.mesh.position.z = -0.51;
  }

  clearAll() {
    this.clearStart();
    this.clearEnd();
    this.clearPath();
  }

  clearStart() {
    this._start.visible = false;
  }

  clearEnd() {
    this._end.visible = false;
  }

  clearPath() {
    this.mesh.remove(this._path);
    this._path = null;
  }

  placeStart({x, y}) {
    this._start.position.x = x;
    this._start.position.y = y;
    this._start.visible = true;
  }

  placeEnd({x, y}, color=0xffffff, scale=1) {
    this._end.position.x = x;
    this._end.position.y = y;
    this._end.visible = true;
    this._end.scale.set(scale, scale, 1);
    this._end.material.color.setHex(color);
  }

  placePath(points, color=0xffffff) {
    this._path = new THREE.Group();

    this._pathMaterial.color.setHex(color);

    points.forEach(({x, y}) => {
      const pathMesh = (new THREE.Mesh(this._pathGeometry, this._pathMaterial));
      pathMesh.position.x = x;
      pathMesh.position.y = y;
      this._path.add(pathMesh);
    });

    this.mesh.add(this._path);
  }
}

export default Overlays;