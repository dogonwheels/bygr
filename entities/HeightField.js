import _ from 'underscore';
import Grid from '../Grid';
import perlin from '../perlin';
import Random from 'random-js';

class HeightField {
  constructor(size, seed) {
    this.heights = new Grid(size);
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        const fx = x / size;
        const fy = y / size;

        this.heights.set({x, y}, perlin(seed, fx - 0.5, fy - 0.5));
      }
    }

    const mt = Random.engines.mt19937();
    mt.seed(seed);
    const { min, range } = this.heights.range();
    const sea = Random.real(0, 0.4)(mt);
    const cliffs = Random.real(0.05, 0.3)(mt);
    const plateau = Random.real(0.3, 0.5)(mt);

    const landLow = range * (sea + cliffs);
    const landHeight = range * plateau;
    const scale = 20 + Random.real(-10, 20)(mt);
    const step = 30;

    this.seaLevel = scale * (-range * cliffs);

    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        let height = this.heights.get({x, y}) - min;

        if (height >= landLow) {
          height = Math.max(height - landHeight, landLow);
        }
        height = landLow + (Math.floor((height - landLow) * step) / step);
        this.heights.set({x, y}, (scale * ((height - landLow))));
      }
    }

    this.free = new Grid(size - 1, 1, 0);
    for (let y = 0; y < size - 1; y++) {
      for (let x = 0; x < size - 1; x++) {
        const h00 = this.heights.get({x, y});
        const h10 = this.heights.get({x:x+1, y});
        const h01 = this.heights.get({x, y:y+1});
        const h11 = this.heights.get({x:x+1, y:y+1});

        const heights = Math.abs(h00) + Math.abs(h10) + Math.abs(h01) + Math.abs(h11);
        const isFree = heights < 0.01;

        this.free.set({x, y}, isFree);
      }
    }

    this.group = new Grid(size - 1, 0, 100);
    for (let y = 0; y < size - 1; y++) {
      for (let x = 0; x < size - 1; x++) {
        if (!this.free.get({x, y})) {
          this.group.set({x, y}, 100)
        }
      }
    }
    let fillSeed = this.findFree();
    let fill = 0;
    const counts = [];
    this.spaces = 0;
    let largestFill;
    while (fillSeed) {
      fill++;
      const count = this.fill(fillSeed, fill);
      counts.push({ fill, count });
      fillSeed = this.findFree();
      if (count > this.spaces) {
        this.spaces = count;
        largestFill = fill;
      }
    }

    this.ground = new Grid(size - 1, false, false);

    for (let y = 0; y < size - 1; y++) {
      let row = '';
      for (let x = 0; x < size - 1; x++) {
        //const cell = this.free.get({x, y});
        const cell = Math.min(this.group.get({x, y}), 100);
        if (cell === largestFill) {
          this.ground.set({x, y}, true)
        }
        row += ' ' + cell;
      }
      //console.log(y + '- ' + row);
    }
  }

  findFree() {
    const size = this.free.width;
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        const inGroup = this.group.get({x, y});
        if (this.free.get({x, y}) && !inGroup) {
          return {x, y};
        }
      }
    }
  }

  fill(seed, color) {
    let count = 0;
    const q = [];
    if (color === this.group.get(seed)) {
      return;
    }
    q.push(seed);
    while (q.length) {
      const n = q.pop();
      const y = n.y;
      let l = n.x;
      for (let x = n.x - 1; x >= 0; x--) {
        if (this.group.get({x, y})) {
          break;
        }
        l = x;
      }
      let r = n.x;
      for (let x = n.x + 1; x < this.group.width; x++) {
        if (this.group.get({x, y})) {
          break;
        }
        r = x;
      }

      for (let x = l; x <= r; x++) {
        count++;
        this.group.set({x, y}, color);
        const above = {x, y: y + 1};
        const below = {x, y: y - 1};
        if (!this.group.get(above)) {
          q.push(above);
        }
        if (!this.group.get(below)) {
          q.push(below);
        }
      }
    }
    return count;
  }
}

export default HeightField;
