import _ from 'underscore';
import THREE from 'three';

const lands = [
  { sea: true, seaColor: 0x333355, seaOpacity: 0.7, landColor: 0x335533},
  { sea: true, seaColor: 0x222222, seaOpacity: 0.8, landColor: 0x332200},
  { sea: false, seaColor: 0x00, seaOpacity: 0.0, landColor: 0x555533},
  { sea: true, seaColor: 0x335533, seaOpacity: 0.5, landColor: 0x553333},
  { sea: true, seaColor: 0x553333, seaOpacity: 1.0, landColor: 0x555555},
  { sea: true, seaColor: 0x8888ff, seaOpacity: 0.8, landColor: 0xcccccc},
];

class LandscapeGeometry extends THREE.PlaneGeometry {
  constructor(size, heightField) {
    super(size, size, size, size);

    this.vertices.forEach(function (vertex) {
      const x = Math.round(vertex.x) + size / 2;
      const y = Math.round(vertex.y) + size / 2;

      vertex.z = heightField.heights.get({x, y});
    });

    this.computeFaceNormals();
    this.computeVertexNormals();
  }
}

const fudge = 0.7;

const leftGeometry = ({heights}) => {
  const g = new THREE.Geometry();
  const s = heights.height / 2;
  for (let y = 0; y < heights.height - 1; y++) {
    const z0 = heights.get({x: 0, y}) + fudge;
    const z1 = heights.get({x: 0, y: y + 1}) + fudge;

    g.vertices.push(new THREE.Vector3(-s, y - s, z0));
    g.vertices.push(new THREE.Vector3(-s, y + 1 - s, z1));
    g.vertices.push(new THREE.Vector3(-s, y + 1 - s, -5));
    g.vertices.push(new THREE.Vector3(-s, y - s, -5));

    const v = y * 4;
    g.faces.push(new THREE.Face3(v, v + 1, v + 2));
    g.faces.push(new THREE.Face3(v, v + 2, v + 3));
  }

  g.computeFaceNormals();
  return g;
};

const rightGeometry = ({heights}) => {
  const g = new THREE.Geometry();
  const s = heights.height / 2;
  for (let x = 0; x < heights.height - 1; x++) {
    const z0 = heights.get({y: 0, x}) + fudge;
    const z1 = heights.get({y: 0, x: x + 1}) + fudge;

    g.vertices.push(new THREE.Vector3(x - s, -s, z0));
    g.vertices.push(new THREE.Vector3(x + 1 - s, -s, z1));
    g.vertices.push(new THREE.Vector3(x + 1 - s, -s, -5));
    g.vertices.push(new THREE.Vector3(x - s, -s, -5));

    const v = x * 4;
    g.faces.push(new THREE.Face3(v, v + 2, v + 1));
    g.faces.push(new THREE.Face3(v, v + 3, v + 2));
  }

  g.computeFaceNormals();
  return g;
};

class Terrain {
  constructor(size, heightField) {
    const land = _(lands).sample();

    const geometry = new LandscapeGeometry(size, heightField);
    const material = new THREE.MeshPhongMaterial({
      color: land.landColor,
      shading: THREE.FlatShading,
      shininess: 0
    });

    this.mesh = new THREE.Group();
    const landMesh = new THREE.Mesh(geometry, material);
    landMesh.pickable = true;
    this.mesh.add(landMesh);

    const leftMesh = new THREE.Mesh(leftGeometry(heightField), material);
    this.mesh.add(leftMesh);
    const rightMesh = new THREE.Mesh(rightGeometry(heightField), material);
    this.mesh.add(rightMesh);

    const seaGeometry = new THREE.PlaneGeometry(size, size, size, size);
    seaGeometry.vertices.forEach(function (vertex) {
      vertex.z = vertex.z + (Math.random() - 0.5) / 5;
    });
    seaGeometry.computeFaceNormals();

    const seaMaterial = new THREE.MeshPhongMaterial({
      color: land.seaColor,
      opacity: land.seaOpacity,
      transparent: true,
      shading: THREE.FlatShading,
      shininess: 0
    });
    const seaMesh = new THREE.Mesh(seaGeometry, seaMaterial);
    seaMesh.position.z = heightField.seaLevel;
    if (land.sea) {
      this.mesh.add(seaMesh);
    }

    this.mesh.position.x = (size / 2) + this.mesh.position.x - 0.5;
    this.mesh.position.y = (size / 2) + this.mesh.position.y - 0.5;
    this.mesh.position.z = this.mesh.position.z - 0.52;

    this.land = landMesh;
  }
}

export default Terrain;