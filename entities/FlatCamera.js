import THREE from 'three';

class FlatCamera extends THREE.OrthographicCamera {
  constructor(scale=1.0) {
    var x = 1;
    var y = 1;

    var width = window.innerWidth;
    var height = window.innerHeight;

    if (width > height) {
      x = width / height;
    } else {
      y = height / width;
    }

    scale *= 0.66;

    x *= scale;
    y *= scale;

    super(-x, x, y, -y, -2 * scale, 2 * scale);

    var ground = new THREE.Matrix4().makeTranslation(0, -(scale / 3), 0);
    var flatten = new THREE.Matrix4().makeRotationX(-Math.PI / 4);
    var spin = new THREE.Matrix4().makeRotationZ(Math.PI / 4);
    var center = new THREE.Matrix4().makeTranslation(-scale / 2, -scale / 2, 0);

    var projectMatrix = this.projectionMatrix.clone();
    //this.projectionMatrix = projectMatrix.multiply(ground).multiply(flatten).multiply(spin).multiply(center);
    this.projectionMatrix = projectMatrix.multiply(ground).multiply(flatten).multiply(spin).multiply(center);
    this.original = this.projectionMatrix.clone();
  }

  pointAt(x, y, scale=1) {
    var zoom = new THREE.Matrix4().makeScale(scale, scale, scale);
    var pan = new THREE.Matrix4().makeTranslation(0.5 - x, 0.5 - y, 0);
    this.projectionMatrix = this.original.clone().multiply(zoom).multiply(pan);
  }
}

export default FlatCamera;