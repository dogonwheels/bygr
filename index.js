import _ from 'underscore';

import THREE from 'three';
import TWEEN from 'tween.js';

import Game from './Game';
import Ui from './Ui';

import Screen from './screens/Screen';
import Welcome from './screens/Welcome';
import Play from './screens/Play';
import Upgrade from './screens/Upgrade';

import StateMachine from './states/StateMachine';

const game = new Game();
const ui = new Ui(game);
const state = new StateMachine();
ui.setStateMachine(state);

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);

const screen = new Screen(new Welcome(), game, state);
//const screen = new Screen(new Play(), game, state);
//const screen = new Screen(new Upgrade(), game, state);
ui.screen = screen;

ui.render();

document.body.appendChild(renderer.domElement);

function animate() {
  requestAnimationFrame(animate);

  screen.current.update();
  state.send('Update');

  if (game.clean()) {
    ui.render();
  }

  TWEEN.update();

  if (screen.current.scene) {
    renderer.render(screen.current.scene, screen.current.camera);
  }
}

animate();

