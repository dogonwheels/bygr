import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';

import Ability from './Ability';

class Heal extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Heal`;

    this.minimumActionPoints = 500;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;

    this.amount = toughness * 10 * (1 + scale);
  }

  can(destination) {
    const adjacent = this._actor.adjacent(destination);
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    return this.isAvailable() && adjacent && _(aliveFriends).findWhere(destination);
  }

  preview(destination) {
    const receiver = this.can(destination);
    return {
      end: receiver,
      actionPoints: receiver && this.minimumActionPoints
    };
  }

  do(destination) {
    const progress = {done: false};

    const receiver = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const hitLocation = {
      x: friend.x + ((receiver.x - friend.x) / 3),
      y: friend.y + ((receiver.y - friend.y) / 3)
    };

    const heal = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to(hitLocation, 100)
      .onUpdate(function () {
        friend.mesh.position.x = this.x;
        friend.mesh.position.y = this.y;
      })
      .onComplete(() => {
        receiver.hit(-this.amount);
      });

    const retreat = new TWEEN.Tween(hitLocation)
      .to({x: friend.x, y: friend.y}, 150)
      .onUpdate(function () {
        friend.mesh.position.x = this.x;
        friend.mesh.position.y = this.y;
      })
      .onComplete(() => {
        progress.done = true;
      });

    const damage = new TWEEN.Tween({scale: 1})
      .to({scale: 1.3}, 500)
      .easing(TWEEN.Easing.Back.In)
      .repeat(1)
      .yoyo(true)
      .onUpdate(function () {
        receiver.mesh.scale.set(this.scale, this.scale, 1)
      });

    heal.chain(retreat, damage);
    heal.start();

    return progress;
  }
}

export default Heal;
