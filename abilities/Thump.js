import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';

import Ability from './Ability';

class Thump extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Thump`;

    this.minimumActionPoints = 400;
    this.spentActionPoints = 1500;
    this.minimumSpentActionPoints = 1500;
    this.damage = strength * 10 * (1 + scale);
  }

  can(destination) {
    const adjacent = this._actor.adjacent(destination);
    const aliveEnemies = _(this._game.enemies).filter((enemy) => (enemy.isAlive()));
    return this.isAvailable() && adjacent && _(aliveEnemies).findWhere(destination);
  }

  preview(destination) {
    const enemy = this.can(destination);
    return {
      end: enemy,
      actionPoints: enemy && this.minimumActionPoints
    };
  }

  do(destination) {
    const progress = {done: false};

    const enemy = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const hitLocation = {
      x: friend.x + ((enemy.x - friend.x) / 3),
      y: friend.y + ((enemy.y - friend.y) / 3)
    };

    const attack = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to(hitLocation, 100)
      .onUpdate(function () {
        friend.mesh.position.x = this.x;
        friend.mesh.position.y = this.y;
      })
      .onComplete(() => {
        enemy.hit(this.amount);
      });

    const retreat = new TWEEN.Tween(hitLocation)
      .to({x: friend.x, y: friend.y}, 150)
      .onUpdate(function () {
        friend.mesh.position.x = this.x;
        friend.mesh.position.y = this.y;
      })
      .onComplete(() => {
        progress.done = true;
      });

    const damage = new TWEEN.Tween({scale: 1})
      .to({scale: 0.1}, 1000)
      .easing(TWEEN.Easing.Back.In)
      .onUpdate(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.scale.set(this.scale, this.scale, 1)
        } else {
          const bounce = Math.abs(0.5 -  this.scale) * 2;
          enemy.mesh.scale.set(bounce, bounce, 1);
        }
      })
      .onComplete(function () {
        enemy.mesh.visible = false;
      });

    attack.chain(retreat, damage);
    attack.start();

    return progress;
  }
}

export default Thump;
