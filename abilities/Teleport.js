import ROT from 'rot-js';
import TWEEN from 'tween.js';

import Ability from './Ability';

class Teleport extends Ability {
  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Teleport`;

    // FIXME: extract to a data source
    this.minimumActionPoints = 200;
    this.range = 20;

    this.minimumSpentActionPoints = 400;
    this.spentActionPoints = this.minimumSpentActionPoints;
  }

  _manhattan(start, end) {
    return Math.abs(start.x - end.x) + Math.abs(start.y - end.y);
  }

  can(destination) {
    const occupied = this._game.map.occupied.get(destination);
    const distance = this._manhattan(this._actor, destination);

    // FIXME: general to one shot actions
    return this.isAvailable() && !occupied && (distance < this.range);
  }

  preview(destination) {
    if (this.can(destination)) {
      return {
        end: destination,
        actionPoints: this.minimumActionPoints
      };
    } else {
      return {};
    }
  }

  do(destination) {
    const progress = { done: false };

    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const disappear = new TWEEN.Tween({ scale: 1 })
      .to({ scale: 0 }, 50)
      .onUpdate(function () {
        friend.mesh.scale.set(this.scale, this.scale, this.scale);
      })
      .onComplete(() => {
        friend.moveTo(destination);
      });

    const reappear = new TWEEN.Tween({ scale: 0 })
      .to({ scale: 1 }, 50)
      .onUpdate(function () {
        // FIXME: updateMesh helper
        friend.mesh.scale.set(this.scale, this.scale, this.scale);
      })
      .onComplete(() => {
        progress.done = true;
      });

    disappear.chain(reappear);
    disappear.start();

    return progress;
  }
}

export default Teleport;
