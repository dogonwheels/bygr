import _ from 'underscore';

import Ability from './Ability';

import hit from '../actions/hit';

class Hit extends Ability {
  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Hit`;

    this.minimumActionPoints = 400;
    this.spentActionPoints = 0;
    this.minimumSpentActionPoints = 0;
    this.damage = strength * 4 * (1 + scale);
  }

  can(destination) {
    const adjacent = this._actor.adjacent(destination);
    const aliveEnemies = _(this._game.enemies).filter((enemy) => (enemy.isAlive()));
    return this.isAvailable() && adjacent && _(aliveEnemies).findWhere(destination);
  }

  preview(destination) {
    const enemy = this.can(destination);
    return {
      end: enemy,
      actionPoints: enemy && this.minimumActionPoints
    };
  }

  do(destination) {
    const enemy = this.can(destination);
    this._actor.useActionPoints(this, this.minimumActionPoints);

    return hit(this._actor, enemy, this.damage);
  }
}

export default Hit;
