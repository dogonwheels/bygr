import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';
import Bullet from '../entities/Bullet';
import colors from '../colors.js';

import Ability from './Ability';
import { distanceBetween } from '../geometry';

class Fireball extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Fireball`;

    this.range = 6;
    this.minimumActionPoints = 500;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;
  }

  can(destination) {
    const aliveEnemies = _(this._game.enemies).filter((enemy) => (enemy.isAlive()));
    const enemy = _(aliveEnemies).findWhere(destination);
    const los = this._actor.lineOfSight(destination);
    const finishes = los.length && distanceBetween(_(los).last(), destination) === 0;
    const within = distanceBetween(destination, this._actor) <= this.range;
    if (finishes && enemy && this.isAvailable() && (los.length > 0) && within){
      return enemy;
    }
  }

  preview(destination) {
    const can = this.can(destination);
    const los = this._actor.lineOfSight(destination);
    return {
      end: destination,
      endColor: can ? colors.target : colors.noTarget,
      path:  los,
      pathColor: can ? colors.path : colors.noPath,
      actionPoints: can && this.minimumActionPoints
    };
  }

  do(destination, scene) {
    const progress = {done: false};

    const enemy = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const bullet = new Bullet();
    scene.add(bullet.mesh);

    const shoot = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to({x: enemy.x, y: enemy.y}, 300)
      .onUpdate(function () {
        bullet.mesh.position.x = this.x;
        bullet.mesh.position.y = this.y;
      })
      .onComplete(() => {
        enemy.hit(1000);
        scene.remove(bullet.mesh);
      });

    const damage = new TWEEN.Tween({scale: 1})
      .to({scale: 0.1}, 400)
      .easing(TWEEN.Easing.Back.In)
      .onUpdate(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.scale.set(this.scale, this.scale, 1)
        } else {
          const bounce = Math.abs(0.5 -  this.scale) * 2;
          enemy.mesh.scale.set(bounce, bounce, 1);
        }
      })
      .onComplete(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.visible = false;
        }
        progress.done = true;
      });

    shoot.chain(damage);
    shoot.start();

    return progress;
  }
}

export default Fireball;
