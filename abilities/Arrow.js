import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';
import Bullet from '../entities/Bullet';

import Ability from './Ability';
import { distanceBetween } from '../geometry';
import colors from '../colors';

class Arrow extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Arrow`;

    this.minimumActionPoints = 500;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;
    this.damage = strength * 10 * (1 + scale);
    this.range = 10;
  }

  can(destination) {
    const aliveEnemies = _(this._game.enemies).filter((enemy) => (enemy.isAlive()));
    const inRange = distanceBetween(destination, this._actor) <= this.range;
    return inRange && this.isAvailable() && _(aliveEnemies).findWhere(destination);
  }

  preview(destination) {
    const can = this.can(destination);
    const path = this._actor.line(destination);
    return {
      end: destination,
      endColor: can ? colors.target : colors.noTarget,
      path: path,
      pathColor: can ? colors.path : colors.noPath,
      actionPoints: can && this.minimumActionPoints
    };
  }

  do(destination, scene) {
    const progress = {done: false};

    const enemy = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const bullet = new Bullet();
    scene.add(bullet.mesh);

    const shoot = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to({x: enemy.x, y: enemy.y}, 100)
      .onUpdate(function () {
        bullet.mesh.position.x = this.x;
        bullet.mesh.position.y = this.y;
      })
      .onComplete(() => {
        enemy.hit(this.damage);
        scene.remove(bullet.mesh);
      });

    const damage = new TWEEN.Tween({scale: 1})
      .to({scale: 0.1}, 400)
      .easing(TWEEN.Easing.Back.In)
      .onUpdate(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.scale.set(this.scale, this.scale, 1)
        } else {
          const bounce = Math.abs(0.5 -  this.scale) * 2;
          enemy.mesh.scale.set(bounce, bounce, 1);
        }
      })
      .onComplete(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.visible = false;
        }
        progress.done = true;
      });

    shoot.chain(damage);
    shoot.start();

    return progress;
  }
}

export default Arrow;
