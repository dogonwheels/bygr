import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';

import colors from '../colors';
import { distanceBetween } from '../geometry';
import Ability from './Ability';

class Move extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = 'Move';
    this.fixedAbility = true;

    this.minimumActionPoints = 200; // Derive cost from a larger valued speed later

    // Can perform action is spent is more than minimum - a minimum of 0 means
    // you can always use the activity
    this.spentActionPoints = 0;
    this.minimumSpentActionPoints = 0;
  }

  _pathActionPoints(path) {
    const cost = Math.max(path.length - 1, 0) * this.minimumActionPoints;
    return cost;
  }

  _pathTo(destination) {
    const occupied = this._game.map.occupied;
    const astar = new ROT.Path.AStar(
      this._actor.x,
      this._actor.y,
      (x, y) => (!occupied.get({ x, y })),
      { topology: 4 }
    );

    const path = [];
    astar.compute(
      destination.x,
      destination.y,
      (x, y) => { path.push({x, y}); }
    );

    return path.reverse().slice(0, Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints) + 1);
  }

  can(destination) {
    const path = this._pathTo(destination);
    return path && path.length;
  }

  preview(destination) {
    const path = this._pathTo(destination);
    const finishes = path.length && (distanceBetween(destination, _(path).last()) === 0);
    return {
      path,
      pathColor: colors.path,
      end: destination,
      endColor: finishes ? colors.target : colors.noTarget,
      actionPoints: this._pathActionPoints(path)
    };
  }

  do(destination) {
    const progress = { done: false };

    // FIXME: ability specifics, not boilerplate
    const path = this._pathTo(destination);
    const friend = this._actor;

    friend.useActionPoints(this, this._pathActionPoints(path));

    const tweens = [];
    for (let p = 1; p < path.length; p++) {
      const start = path[p - 1];
      const destination = path[p];
      const moveTween = new TWEEN.Tween(start)
        .to(path[p], 200)
        .onUpdate(function () {
          friend.mesh.position.x = this.x;
          friend.mesh.position.y = this.y;
        })
        .onComplete(() => {
          friend.moveTo(destination);
        });

      tweens.push(moveTween);
    }
    // /ability specifics

    // Always mark ourselves as done
    const finalTween = new TWEEN.Tween({})
      .to({}, 10)
      .onComplete(() => {
        progress.done = true;
      });

    tweens.push(finalTween);
    for (let i = 1; i < tweens.length; i++) {
      tweens[i - 1].chain(tweens[i]);
    }
    tweens[0].start();

    return progress;
  }
}

export default Move;
