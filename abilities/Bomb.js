import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';
import Bullet from '../entities/Bullet';
import Explosion from '../entities/Explosion';

import Ability from './Ability';
import colors from '../colors';

import { distanceBetween } from '../geometry';

class Bomb extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { level, minimumXp, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Bomb`;

    this.minimumActionPoints = 500;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;
    this.damage = strength * 10 * (1 + scale);
    this.range = 8;
    this.splash = 3;
  }

  can(destination) {
    const los = this._actor.lineOfSight(destination);
    const finishes = los.length && distanceBetween(_(los).last(), destination) === 0;
    const within = distanceBetween(destination, this._actor) <= this.range;
    if (finishes && this.isAvailable() && (los.length > 0) && within) {
      return destination;
    }
  }

  preview(destination) {
    const can = this.can(destination);
    const los = this._actor.lineOfSight(destination);
    return {
      end: destination,
      endColor: can ? colors.target : colors.noTarget,
      endScale: this.splash,
      path: can ? los : [] ,
      pathColor: can ? colors.path : colors.noPath,
      actionPoints: can && this.minimumActionPoints
    };
  }

  do(destination, scene) {
    const progress = {done: false};

    const target = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const bullet = new Bullet();
    scene.add(bullet.mesh);

    const shoot = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to(target, 500)
      .onUpdate(function () {
        bullet.mesh.position.x = this.x;
        bullet.mesh.position.y = this.y;
      })
      .onComplete(() => {
        scene.remove(bullet.mesh);
        this._game.enemies.forEach((enemy) => {
          const alive = enemy.isAlive();
          const distance = distanceBetween(enemy, target);
          if (alive && (distance <= this.splash)) {
            enemy.hit(this.damage);
            if (!enemy.isAlive()) {
              enemy.mesh.visible = false;
            }
          }
        });
      });

    const explosion = new Explosion();
    explosion.mesh.position.x = target.x;
    explosion.mesh.position.y = target.y;
    explosion.mesh.visible = false;
    scene.add(explosion.mesh);
    const explode = new TWEEN.Tween({scale: 0.2, opacity: 1, green: 1})
      .to({scale: 3, opacity: 0, green: 0}, 500)
      .easing(TWEEN.Easing.Exponential.Out)
      .onUpdate(function () {
        explosion.mesh.visible = true;
        explosion.mesh.material.color.setRGB(1, this.green, 0);
        explosion.mesh.material.opacity = this.opacity;
        explosion.mesh.scale.set(this.scale, this.scale, this.scale);
      })
      .onComplete(() => {
        scene.remove(explosion.mesh);
        progress.done = true;
      });

    shoot.chain(explode);
    shoot.start();

    return progress;
  }
}

export default Bomb;
