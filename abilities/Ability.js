class Ability {
  constructor(game, actor, spec) {
    this._game = game;
    this._actor = actor;
    this._spec = spec;

    this.reset();
  }

  minimumXp() {
    return this._spec.minimumXp;
  }

  isSufficientLevel() {
    return !this._spec.minimumXp || (this._actor.stats.xp >= this._spec.minimumXp);
  }

  isAvailable() {
    const waiting = this.spentActionPoints < this.minimumSpentActionPoints;
    const enoughActionPoints = this.minimumActionPoints <= this._actor.remainingActionPoints;

    return enoughActionPoints && this.isSufficientLevel() && !waiting;
  }
}

export default Ability;