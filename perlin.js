import Random from 'random-js';

var mt = Random.engines.mt19937();
var cache = {};
//var seed = 2611;
var seed = 0;
//var seed = 148.85987644083798;
//var seed = 20100213;
//var seed = 20150613;

function noise(x, y) {
  var key = `${x}.${y}`;

  if (cache[key] === undefined) {
    mt.seed((x * seed) + y);
    cache[key] = Random.real(0, 1)(mt);
  }

  return cache[key];
}

function splitDecimal(value) {
  var whole = Math.floor(value);
  return {
    whole,
    fraction: value -  whole
  };
}

function smoothNoise(x, y) {
  var corners = (noise(x - 1, y - 1) + noise(x + 1, y - 1) + noise(x - 1, y + 1) + noise(x + 1, y + 1)) / 16;
  var sides = (noise(x - 1, y) + noise(x + 1, y) + noise(x, y - 1) + noise(x, y + 1)) / 8;
  var center = noise(x, y) / 4;
  return corners + sides + center;
}

function interpolate(a, b, fraction) {
  var ft = fraction * Math.PI;
  var f = (1 - Math.cos(ft)) * .5;

  return a * (1 - f) + b * f;
}

function interpolateNoise(x, y) {
  var { whole: wholeX, fraction: fractionX } = splitDecimal(x);
  var { whole: wholeY, fraction: fractionY } = splitDecimal(y);

  var v1 = smoothNoise(wholeX, wholeY);
  var v2 = smoothNoise(wholeX + 1, wholeY);
  var v3 = smoothNoise(wholeX, wholeY + 1);
  var v4 = smoothNoise(wholeX + 1, wholeY + 1);

  var i1 = interpolate(v1, v2, fractionX);
  var i2 = interpolate(v3, v4, fractionX);

  return interpolate(i1, i2, fractionY)
}

function perlin(currentSeed, x, y) {
  seed = currentSeed;
  var total = 0;
  var p = 0.7; // 0.6 to 0.8
  var n = 6;

  cache = {};
  for (var i = 0; i < n; i++) {
    var frequency = Math.pow(2, i);
    var amplitude = Math.pow(p, i);

    total += interpolateNoise(x * frequency, y * frequency) * amplitude;
  }
  return total;
}

export default perlin;
