import Move from './abilities/Move';
import Hit from './abilities/Hit';
import Thump from './abilities/Thump';
import Arrow from './abilities/Arrow';
import Fireball from './abilities/Fireball';
import Heal from './abilities/Heal';
import Bomb from './abilities/Bomb';
import Teleport from './abilities/Teleport';

const friends = {
  cub: {
    stats: [2, 6, 8, 4],
    abilities: [
      [Move, 1, 0],
      [Hit, 1, 0],
      [Thump, 1, 0],
      [Bomb, 1, 15]
    ]
  },
  octa: {
    stats: [4, 8, 2, 6],
    abilities: [
      [Move, 1, 0],
      [Hit, 0.25, 0],
      [Heal, 1, 0],
      [Bomb, 1, 5]
    ]
  },
  oid: {
    stats: [8, 4, 6, 2],
    abilities: [
      [Move, 1, 0],
      [Hit, 0.5, 0],
      [Arrow, 1, 0]
    ]
  },
  tetra: {
    stats: [6, 2, 4, 8],
    abilities: [
      [Move, 1, 0],
      [Hit, 0.25, 0],
      [Fireball, 1, 0],
      [Teleport, 1, 5]
    ]
  }
};

export default friends;
