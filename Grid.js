import _ from 'underscore';

class Grid {
  constructor(size, value, outside) {
    this.width = size;
    this.height = size;

    this._value = value;
    this._outside = outside;

    this._cells = [];
    for (let y = 0; y < size; y++) {
      const row = [];
      for (let x = 0; x < size; x++) {
        row.push(value);
      }
      this._cells.push(row);
    }
  }

  range() {
    const allCells = _(this._cells).flatten();

    allCells.sort((a, b) => (a - b));

    const min = allCells[0];
    const max = _(allCells).last();
    const range =  max - min;

    return { min, max, range };
  }

  get({x, y}) {
    if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
      return this._cells[y][x];
    } else {
      return this._outside;
    }
  }

  set({x, y}, value) {
    if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
      this._cells[y][x] = value;
    }
  }
}

export default Grid;
