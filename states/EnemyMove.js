import EnemyTurn from './EnemyTurn';

class EnemyMove {
  constructor(context, decision) {
    this._context = context;
    this._scene = context.scene;
    this._ability = decision.ability;
    this._destination = decision.destination;
  }

  enter() {
    this._progress = this._ability.do(this._destination, this._scene);
  }

  onUpdate() {
    if (this._progress.done) {
      return new EnemyTurn(this._context);
    }
  }
}

export default EnemyMove;