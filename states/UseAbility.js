import SetupFriendAbility from './SetupFriendAbility';

class UseAbility {
  constructor(context, friend, ability, destination) {
    this._context = context;
    this._scene = context.scene;
    this._friend = friend;
    this._ability = ability;
    this._destination = destination;
  }

  enter() {
    this._progress = this._ability.do(this._destination, this._scene);
  }

  onUpdate() {
    if (this._progress.done) {
      return new SetupFriendAbility(this._context, this._friend, this._ability);
    }
  }
}

export default UseAbility;