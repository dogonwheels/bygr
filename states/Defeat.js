class Defeat {
  constructor(context) {
    this._context = context;
  }

  enter() {
    this._context.game.progress = 'defeat';
  }
}

export default Defeat;