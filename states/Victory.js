import _ from 'underscore';

class Victory {
  constructor(context) {
    this._context = context;
  }

  enter() {
    // Split up XP and points
    const game = this._context.game;

    for (let i = 0; i < game.reward; i++) {
      const friend = _(game.friends).sample();
      friend.stats.xp += 1;
      friend.stats.xpToSpend += 1;
    }

    game.progress = 'victory';
  }
}

export default Victory;