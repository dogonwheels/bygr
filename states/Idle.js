class Idle {
  constructor(context) {
    this._context = context;
  }

  enter() {
  }

  exit() {
  }

  onSelect() {
  }
}

export default Idle;