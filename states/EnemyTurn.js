import _ from 'underscore';
import EnemyMove from './EnemyMove';
import SetupFriendAbility from './SetupFriendAbility';

class EnemyTurn {
  constructor(context) {
    this._context = context;
  }

  enter() {
    // Get available abilities
    const allAbilities = this._context.game.enemies.map((enemy) => {
      if (!enemy.isAlive()) {
        return { enemy, abilities: [] };
      }
      const abilities = enemy.availableAbilities();
      return { enemy, abilities };
    });
    const availableEnemies = allAbilities.filter(({abilities}) => (abilities.length));

    // Now have a list of { enemy, abilities } with an ordered set of abilities
    // that we have enough AP for - go in turn until we find a viable one
    for (const { abilities } of availableEnemies) {
      for (const ability of abilities) {
        // An ability the actor 'can' and should do
        const destination = ability.suggest();

        if (destination) {
          this._decision = { ability, destination };
          break;
        }
      }

      if (this._decision) {
        break;
      }
    }
  }

  onUpdate() {
    if (this._decision) {
      return new EnemyMove(this._context, this._decision);
    } else {
      this._context.game.friends.forEach((friend) => (friend.resetActionPoints()));
      return new SetupFriendAbility(this._context);
    }
  }
}

export default EnemyTurn;