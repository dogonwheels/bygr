class StateMachine {
  send(input, options) {
    const handler = this._state['on' + input];
    if (handler) {
      this.transition(handler.call(this._state, options));
    }
  }

  transition(nextState) {
    if (nextState && nextState !== this._state) {
      if (this._state && this._state.exit) this._state.exit();
      this._state = nextState;
      if (this._state.enter) this._state.enter();
    }
  }
}

export default StateMachine;
