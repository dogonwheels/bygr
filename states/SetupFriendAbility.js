import ROT from 'rot-js';
import UseAbility from './UseAbility';
import EnemyTurn from './EnemyTurn';
import Victory from './Victory';
import Defeat from './Defeat';

class SetupFriendAbility {
  constructor(context, friend, ability) {
    // FIXME: select next best friend and ability
    friend = friend || context.game.friends[0];

    this._context = context;
    this._friend = friend;
    this._ability = ability || friend.abilities[0];
  }

  enter() {
    this._context.game.currentFriend = this._friend;
    this._context.game.currentAbility = this._ability;
    this._context.game.dirty();

    const { x, y } = this._friend;

    if (this._friend.isAlive()) {
      this._context.overlays.placeStart({x, y});
    }
  }

  exit() {
    this._context.overlays.clearAll();

    this._friend.previewActionPoints(0);
  }

  onSelectFriend({ friend }) {
    return new SetupFriendAbility(this._context, friend);
  }

  onSelectAbility({ ability }) {
    return new SetupFriendAbility(this._context, this._friend, ability);
  }

  onTileSelect(destination) {
    if (!this._friend.isAlive()) {
      // FIXME: switch to an alive one?
      return;
    }

    if (this._ability.can(destination)) {
      return new UseAbility(this._context, this._friend, this._ability, destination);
    }
  }

  onTileHover(destination) {
    if (!this._friend.isAlive()) {
      // FIXME: switch to an alive one?
      return;
    }

    const preview = this._ability.preview(destination);

    if (preview.path) {
      this._context.overlays.clearPath();
      this._context.overlays.placePath(preview.path, preview.pathColor);
    }

    if (preview.end) {
      this._context.overlays.placeEnd(preview.end, preview.endColor, preview.endScale);
    } else {
      this._context.overlays.clearEnd();
    }

    if (preview.actionPoints !== undefined) {
      this._friend.previewActionPoints(preview.actionPoints);
    }
  }

  onGameHover() {
    this._context.overlays.clearEnd();
    this._context.overlays.clearPath();

    this._friend.previewActionPoints(0);
  }

  onEndTurn() {
    this._context.game.enemies.forEach((friend) => (friend.resetActionPoints()));
    return new EnemyTurn(this._context);
  }

  onUpdate() {
    const aliveEnemies = this._context.game.enemies.filter((enemy) => (enemy.isAlive()));
    if (!aliveEnemies.length) {
      return new Victory(this._context);
    }
    const aliveFriends = this._context.game.friends.filter((friend) => (friend.isAlive()));
    if (!aliveFriends.length) {
      return new Defeat(this._context);
    }
  }
}

export default SetupFriendAbility;
