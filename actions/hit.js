import TWEEN from 'tween.js';

const hit = (attacker, defender, damage) => {
  const progress = {done: false};
  const hitLocation = {
    x: attacker.x + ((defender.x - attacker.x) / 3),
    y: attacker.y + ((defender.y - attacker.y) / 3)
  };

  const attack = new TWEEN.Tween({x: attacker.x, y: attacker.y})
    .to(hitLocation, 100)
    .onUpdate(function () {
      attacker.mesh.position.x = this.x;
      attacker.mesh.position.y = this.y;
    })
    .onComplete(() => {
      defender.hit(damage);
    });

  const retreat = new TWEEN.Tween(hitLocation)
    .to({x: attacker.x, y: attacker.y}, 150)
    .onUpdate(function () {
      attacker.mesh.position.x = this.x;
      attacker.mesh.position.y = this.y;
    });

  const damaging = new TWEEN.Tween({scale: 1})
    .to({scale: 0.1}, 500)
    .easing(TWEEN.Easing.Back.In)
    .onUpdate(function () {
      if (defender.isAlive()) {
        const bounce = Math.abs(0.5 - this.scale) + 0.5;
        defender.mesh.scale.set(bounce, bounce, 1);
      } else {
        defender.mesh.scale.set(this.scale, this.scale, 1)
      }
    })
    .onComplete(() => {
      if (!defender.isAlive()) {
        defender.mesh.visible = false;
        defender.mesh.scale.set(1, 1, 1)
      }
      progress.done = true;
    });

  attack.chain(retreat, damaging);
  attack.start();

  return progress;
};

export default hit;
