import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';
import Bullet from '../entities/Bullet';
import Explosion from '../entities/Explosion';

import Ability from '../abilities/Ability';
import colors from '../colors';

import { distanceBetween } from '../geometry';

class Explode extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { range, scale } = this._spec;
    const { agility, toughness, strength, vitality } = this._actor.stats;

    this.minimumActionPoints = 500;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;
    this.damage = strength * 5 * (1 + scale);
    this.splash = 3;
  }

  can(destination) {
    const adjacent = this._actor.adjacent(destination);
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    return this.isAvailable() && adjacent && _(aliveFriends).findWhere(destination);
  }

  suggest() {
    console.log('looking for neighbor');
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    const neighbours = aliveFriends.filter((friend) => (this._actor.adjacent(friend)));
    const suggestedDestination = _(neighbours).sample();

    if (suggestedDestination) {
      console.log('found someone at ', suggestedDestination);
      return this.can(suggestedDestination);
    }
  }

  do(destination, scene) {
    const progress = {done: false};

    const target = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const explosion = new Explosion();
    explosion.mesh.position.x = target.x;
    explosion.mesh.position.y = target.y;
    explosion.mesh.visible = false;
    scene.add(explosion.mesh);

    friend.hit(this.damage);
    if (!friend.isAlive()) {
      friend.mesh.visible = false;
    }
    this._game.friends.forEach((enemy) => {
      const alive = enemy.isAlive();
      const distance = distanceBetween(enemy, target);
      if (alive && (distance <= this.splash)) {
        enemy.hit(this.damage);
        if (!enemy.isAlive()) {
          enemy.mesh.visible = false;
        }
      }
    });

    const explode = new TWEEN.Tween({scale: 0.2, opacity: 1, green: 1})
      .to({scale: 3, opacity: 0, green: 0}, 500)
      .easing(TWEEN.Easing.Exponential.Out)
      .onUpdate(function () {
        explosion.mesh.visible = true;
        explosion.mesh.material.color.setRGB(1, this.green, 0);
        explosion.mesh.material.opacity = this.opacity;
        explosion.mesh.scale.set(this.scale, this.scale, this.scale);
      })
      .onComplete(() => {
        scene.remove(explosion.mesh);
        progress.done = true;
      });

    explode.start();

    return progress;
  }
}

export default Explode;
