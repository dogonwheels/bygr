import ROT from 'rot-js';
import TWEEN from 'tween.js';

import Ability from '../abilities/Ability';

class Move extends Ability {
  reset() {
    const { scale, range } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.minimumActionPoints = 200;

    // Can perform action is spent is more than minimum - a minimum of 0 means
    // you can always use the activity
    this.spentActionPoints = 0;
    this.minimumSpentActionPoints = 0;
  }

  _pathActionPoints(path) {
    const cost = Math.max(path.length - 1, 0) * this.minimumActionPoints;
    return cost;
  }

  can(destination) {
    // The end of a move must be clear
    const path = this._actor.path(destination, false);

    const reach = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);

    // Includes start and end cell
    return path && path.length && path.length < (reach + 2);
  }

  do(destination) {
    const progress = { done: false };

    const friend = this._actor;
    const path = friend.path(destination);

    friend.useActionPoints(this, this._pathActionPoints(path));

    const tweens = [];
    for (let p = 1; p < path.length; p++) {
      const start = path[p - 1];
      const destination = path[p];
      const moveTween = new TWEEN.Tween(start)
        .to(path[p], 200)
        .onUpdate(function () {
          friend.mesh.position.x = this.x;
          friend.mesh.position.y = this.y;
        })
        .onComplete(() => {
          friend.moveTo(destination);
        });

      tweens.push(moveTween);
    }
    // /ability specifics

    // Always mark ourselves as done
    const finalTween = new TWEEN.Tween({})
      .to({}, 10)
      .onComplete(() => {
        progress.done = true;
      });

    tweens.push(finalTween);
    for (let i = 1; i < tweens.length; i++) {
      tweens[i - 1].chain(tweens[i]);
    }
    tweens[0].start();

    return progress;
  }
}

export default Move;
