import _ from 'underscore';

import Move from './Move';

class MoveToWeakest extends Move {
  suggest() {
    console.log('Trying to move to weakest');
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    const paths = aliveFriends.map((friend) => ({ friend, path: this._actor.path(friend) }));
    const viablePaths = paths.filter(({path}) => (path.length > 2));

    let leastHealth = 5000;
    let suggestion;
    viablePaths.forEach(({friend, path}) => {
      if (friend.health < leastHealth) {
        suggestion = path;
        leastHealth = friend.health;
      }
    });

    if (suggestion && suggestion.length < this._spec.range) {
      const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);
      const suggestedDestination = suggestion[maximumSteps] || _(suggestion).last();
      console.log(`can go ${maximumSteps} found nearest at ${suggestedDestination}`);
      if (this.can(suggestedDestination)) {
        return suggestedDestination;
      }
    }
  }
}

export default MoveToWeakest;
