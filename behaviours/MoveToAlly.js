import _ from 'underscore';

import Move from './Move';

class MoveToAlly extends Move {
  suggest() {
    console.log('Trying to move to allies');
    const aliveAllies = _(this._game.enemies).filter((ally) => (ally.isAlive()));
    const paths = aliveAllies.map((ally) => (this._actor.path(ally)));
    const viablePaths = paths.filter((path) => (path.length > 2));

    const shortestToLongest = _(viablePaths).sortBy('length');
    const suggestion = _(shortestToLongest).first();
    const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);

    if (suggestion && suggestion.length < this._spec.range) {
      const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);
      const suggestedDestination = suggestion[maximumSteps] || _(suggestion).last();
      console.log(`can go ${maximumSteps} found nearest at ${suggestedDestination}`);
      if (this.can(suggestedDestination)) {
        return suggestedDestination;
      }
    }
  }
}

export default MoveToAlly;
