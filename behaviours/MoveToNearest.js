import _ from 'underscore';

import Move from './Move';

class MoveToNearest extends Move {
  suggest() {
    console.log('Trying to move to nearest');
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    const paths = aliveFriends.map((friend) => (this._actor.path(friend)));
    const viablePaths = paths.filter((path) => (path.length > 2));

    const shortestToLongest = _(viablePaths).sortBy('length');
    const suggestion = _(shortestToLongest).first();
    const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);

    if (suggestion && suggestion.length < this._spec.range) {
      const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);
      const suggestedDestination = suggestion[maximumSteps] || _(suggestion).last();
      console.log(`can go ${maximumSteps} found nearest at ${suggestedDestination}`);
      if (this.can(suggestedDestination)) {
        return suggestedDestination;
      }
    }
  }
}

export default MoveToNearest;
