import _ from 'underscore';

import Ability from '../abilities/Ability';

import hit from '../actions/hit';

class Hit extends Ability {
  reset() {
    const { scale } = this._spec;
    const { agility, toughness, strength, vitality } = this._actor.stats;

    this.minimumActionPoints = 500;
    this.spentActionPoints = 0;
    this.minimumSpentActionPoints = 0;
    this.damage = strength * 4 * (1 + scale);
  }

  can(destination) {
    const adjacent = this._actor.adjacent(destination);
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    return this.isAvailable() && adjacent && _(aliveFriends).findWhere(destination);
  }

  suggest() {
    console.log('looking for neighbor');
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    const neighbours = aliveFriends.filter((friend) => (this._actor.adjacent(friend)));
    const suggestedDestination = _(neighbours).sample();

    if (suggestedDestination) {
      console.log('found someone at ', suggestedDestination);
      return this.can(suggestedDestination);
    }
  }

  do(destination) {
    const friend = this.can(destination);
    this._actor.useActionPoints(this, this.minimumActionPoints);

    return hit(this._actor, friend, this.damage);
  }
}

export default Hit;
