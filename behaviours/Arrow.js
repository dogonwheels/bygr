import _ from 'underscore';
import ROT from 'rot-js';
import TWEEN from 'tween.js';
import Bullet from '../entities/Bullet';

import Ability from '../abilities/Ability';
import { distanceBetween } from '../geometry';
import colors from '../colors';

class Arrow extends Ability {
  constructor(game, actor, spec) {
    super(game, actor, spec);
  }

  reset() {
    const { range, scale } = this._spec;
    const { agility, toughness, strength, vitality, xp } = this._actor.stats;

    this.name = `Arrow`;

    this.minimumActionPoints = 600;
    this.spentActionPoints = 1000;
    this.minimumSpentActionPoints = 1000;
    this.damage = strength * 5 * (1 + scale);
    this.range = range;
  }

  suggest() {
    const aliveFriends = _(this._game.friends).filter(
      (friend) => (friend.isAlive())
    );
    const inRangeFriends = aliveFriends.filter(
      (friend) => (distanceBetween(friend, this._actor) < this.range)
    );

    const suggestion = _(inRangeFriends).first();

    if (suggestion && this.can(suggestion)) {
      return suggestion;
    }
  }

  can(destination) {
    const aliveFriends = _(this._game.friends).filter((friend) => (friend.isAlive()));
    const inRange = distanceBetween(destination, this._actor) <= this.range;
    return inRange && this.isAvailable() && _(aliveFriends).findWhere(destination);
  }

  do(destination, scene) {
    const progress = {done: false};

    const enemy = this.can(destination);
    const friend = this._actor;
    friend.useActionPoints(this, this.minimumActionPoints);

    const bullet = new Bullet();
    scene.add(bullet.mesh);

    const shoot = new TWEEN.Tween({x: friend.x, y: friend.y})
      .to({x: enemy.x, y: enemy.y}, 400)
      .onUpdate(function () {
        bullet.mesh.position.x = this.x;
        bullet.mesh.position.y = this.y;
      })
      .onComplete(() => {
        enemy.hit(this.damage);
        scene.remove(bullet.mesh);
      });

    const damage = new TWEEN.Tween({scale: 1})
      .to({scale: 0.1}, 400)
      .easing(TWEEN.Easing.Back.In)
      .onUpdate(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.scale.set(this.scale, this.scale, 1)
        } else {
          const bounce = Math.abs(0.5 -  this.scale) * 2;
          enemy.mesh.scale.set(bounce, bounce, 1);
        }
      })
      .onComplete(function () {
        if (!enemy.isAlive()) {
          enemy.mesh.visible = false;
        }
        enemy.mesh.scale.set(1, 1, 1);
        progress.done = true;
      });

    shoot.chain(damage);
    shoot.start();

    return progress;
  }
}

export default Arrow;
