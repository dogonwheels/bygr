import _ from 'underscore';

import Move from './Move';
import { distanceBetween } from '../geometry';

class Retreat extends Move {
  suggest() {
    console.log('Trying to move away');

    const sight = this._spec.range;
    const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);

    const totalDistance = (position) => {
      let total = 0;
      _(this._game.friends).forEach((friend) => {
        const inRange = Math.min(distanceBetween(friend, position), sight);
        total += inRange;
      });
      return total;
    };

    const {x, y} = this._actor;
    const current = totalDistance({x, y});

    let walks = [{x:x-1, y}, {x, y:y-1}, {x, y:y+1}, {x:x+1, y}];
    walks = _(walks).shuffle();

    let suggestion;
    walks.forEach((walk) => {
      const next = totalDistance(walk);
      if ((next > current) && this.can(walk)) {
        suggestion = walk;
      }
    });

    if (suggestion && maximumSteps > 0) {
      console.log('running away to ', suggestion, x, y);
      return suggestion;
    }
  }
}

export default Retreat;
