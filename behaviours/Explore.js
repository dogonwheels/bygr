import _ from 'underscore';

import Move from './Move';

class Explore extends Move {
  suggest() {
    console.log('Trying to explore');

    const attempts = 10;
    let destination;
    for (let attempt = 0; attempt < attempts; attempt++) {
      const x = Math.floor(this._game.mapSize * Math.random());
      const y = Math.floor(this._game.mapSize * Math.random());

      if (!this._game.map.occupied.get({x, y})) {
        destination = {x, y};
        break;
      }
    }

    if (!destination) {
      return;
    }

    const suggestion = this._actor.path(destination);
    const maximumSteps = Math.floor(this._actor.remainingActionPoints / this.minimumActionPoints);

    if (suggestion && suggestion.length) {
      const suggestedDestination = suggestion[maximumSteps] || _(suggestion).last();
      console.log(`can go ${maximumSteps} pottering`);
      if (this.can(suggestedDestination)) {
        return suggestedDestination;
      }
    }
  }
}

export default Explore;
