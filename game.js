import _ from 'underscore';
import Random from 'random-js';
import Cub from './actors/Cub';
import Octa from './actors/Octa';
import Oid from './actors/Oid';
import Tetra from './actors/Tetra';
import { Grunt, Warrior, Archer, Ogre, Lemming, Wizard, Slime, Slimet, Doc } from './actors/Enemies';
import SpawnPoints from './entities/SpawnPoints';
import HeightField from './entities/HeightField';

import Grid from './Grid';

const costs = [
  { who: Grunt, cost: 1 },
  { who: Warrior, cost : 2 },
  { who: Archer, cost: 2 },
  { who: Ogre, cost: 4 },
  { who: Lemming, cost: 4 },
  { who: Wizard, cost: 8 },
  { who: Slime, cost: 8 },
  { who: Slimet, cost: 4 },
  { who: Doc, cost: 10 }
];

class Game {
  constructor() {
    this.level = {
      difficulty: 1
    };
    this.mapSize = 24;
    this.map = {
      occupied: new Grid(this.mapSize, false, true) // Inside is not occupied, outside is
    };
    this.friends = [
      new Tetra(this),
      new Oid(this),
      new Octa(this),
      new Cub(this)
    ];
    this.enemies = [
    ]
  }

  startLevel(seed) {
    this.progress = 'play';

    let level = 10;
    this.friends.forEach((friend) => {
      level += friend.stats.xp;
    });

    const difficulty = this.level.difficulty;

    const points = difficulty + level;
    this.reward = difficulty * 2;
    let pointsLeft = points;

    const mt = Random.engines.mt19937();
    mt.seed(seed);


    const counts = {};
    const groups = [];
    while (pointsLeft > 0) {
      let toSpend = Random.integer(1, pointsLeft)(mt);
      pointsLeft -= toSpend;
      const group = [];
      while (toSpend) {
        const available = _(costs).filter(({cost}) => (cost <= toSpend));
        const sample = _(available).sample();
        //const Enemy = sample.who;
        const Enemy = Archer;
        counts[sample.who] = counts[sample.who] || 1;
        const enemy = new Enemy(this, counts[sample.who]++);
        this.enemies.push(enemy);
        group.push(enemy);
        toSpend -= sample.cost;
      }
      groups.push(group);
    }

    const enemyCount = this.enemies.length;

    do {
      const heightSeed = Random.integer(10000, 40000)(mt);
      this.heightField = new HeightField(this.mapSize + 1, heightSeed)
    } while (this.heightField.spaces < (50 + (enemyCount + 5)));

    const ground = this.heightField.ground;
    for (let y = 0; y < this.mapSize; y++) {
      for (let x = 0; x < this.mapSize; x++) {
        this.map.occupied.set({x, y}, !ground.get({x, y}));
      }
    }

    const spawn = new SpawnPoints(ground, seed);

    groups.forEach((group) => {
      const enemySpawns = spawn.fetch(group.length);
      group.forEach((enemy, index) => {
        enemy.moveTo(enemySpawns[index]);
        enemy.reset();
      });
    });

    const friendSpawns = spawn.fetch(4, 3);
    this.friends.forEach((friend, index) => {
      friend.moveTo(friendSpawns[index]);
      friend.reset()
    });
  }

  dirty() {
    this._needsUpdate = true;
  }

  clean() {
    const result = this._needsUpdate;
    this._needsUpdate = false;
    return result;
  }
}

export default Game;

