const distanceBetween = (p1, p2) => {
  const ax = Math.pow(p1.x - p2.x, 2);
  const ay = Math.pow(p1.y - p2.y, 2);

  return Math.sqrt(ax + ay);
};

export { distanceBetween };